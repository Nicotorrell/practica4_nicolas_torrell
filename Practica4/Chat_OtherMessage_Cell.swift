//
//  Chat_OtherMessage_Cell.swift
//  Practica4
//
//  Created by dedam on 3/2/18.
//  Copyright © 2018 dedam. All rights reserved.
//

import UIKit
import Foundation

class Chat_OtherMessage_Cell: UITableViewCell {
    var id:Int = 0
    @IBOutlet weak var expandableBubble: UIImageView!
    @IBOutlet weak var contentLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
