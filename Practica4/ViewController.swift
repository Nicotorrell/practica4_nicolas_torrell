//
//  ViewController.swift
//  Practica4
//
//  Created by dedam on 1/2/18.
//  Copyright © 2018 dedam. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate,
UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var editText: UITextField!
    @IBOutlet weak var toolbar: UIView!
    let imagePicker = UIImagePickerController()
    @IBOutlet weak var tablavista: UITableView!
    var m_aMessage:[[ChatData]] = [];
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        imagePicker.delegate = self
        self.tablavista.dataSource = self
        self.tablavista.delegate = self
        self.tablavista.separatorStyle = UITableViewCellSeparatorStyle.none
        loadInicialChat()
        // Do any additional setup after loading the view, typically from a nib.
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                //keyboardSize.height sale la primera vez con height=258 y al cerra con height=258 pero las siguientes veces al aparecer sale con height=216 y cierra con height=258
                self.toolbar.frame.origin.y -= keyboardSize.height
                self.tablavista.frame.size.height -= keyboardSize.height
        }
    }
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                self.toolbar.frame.origin.y += keyboardSize.height
                self.tablavista.frame.size.height += keyboardSize.height
        }
    }
    
    func loadInicialChat(){
        var section5:[ChatData] = []
        for i in 0..<1{
            let newChat:ChatData = ChatData.init()
            newChat.m_eChatDataType = ChatData.ChatDataType.ChatData_Message
            newChat.id = i
            newChat.m_bIsMine = true
            newChat.m_sMessage = "Hola, que tal"
            
            
            section5.append(newChat)
        }
        var section6:[ChatData] = []
        for i in 0..<1{
            let newChat:ChatData = ChatData.init()
            newChat.m_eChatDataType = ChatData.ChatDataType.ChatData_Message
            newChat.id = i
            newChat.m_bIsMine = false
            
            newChat.m_sMessage = "Bien, y tu"
            
            
            section6.append(newChat)
        }
        
        var section7:[ChatData] = []
        for i in 0..<1{
            let newChat:ChatData = ChatData.init()
            newChat.m_eChatDataType = ChatData.ChatDataType.ChatData_Image
            newChat.id = i
            newChat.m_bIsMine = false
            newChat.imgName = "company"
            newChat.m_sMessage = ""
            
            
            section7.append(newChat)
        }
        var section8:[ChatData] = []
        for i in 0..<1{
            let newChat:ChatData = ChatData.init()
            newChat.m_eChatDataType = ChatData.ChatDataType.ChatData_Image
            newChat.id = i
            newChat.m_bIsMine = true
            newChat.imgName = "company"
            newChat.m_sMessage = ""
            
            
            section8.append(newChat)
        }
        m_aMessage.append(section5)
        m_aMessage.append(section6)
        m_aMessage.append(section7)
        m_aMessage.append(section8)
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func enviar_Button(_ sender: Any) {
        
        view.endEditing(true)
        var section5:[ChatData] = []
        if(editText.text != ""){
            for i in 0..<1{
                let newChat:ChatData = ChatData.init()
                newChat.m_eChatDataType = ChatData.ChatDataType.ChatData_Message
                newChat.id = i
                newChat.m_bIsMine = true
                newChat.m_sMessage = editText.text!
                
                section5.append(newChat)
                }
            }else{
                for i in 0..<1{
                    let newChat:ChatData = ChatData.init()
                    newChat.m_eChatDataType = ChatData.ChatDataType.ChatData_Message
                    newChat.id = i
                    newChat.m_bIsMine = true
                    newChat.m_sMessage = "mensaje test"
                    
                    section5.append(newChat)
            }
        }
        var section6:[ChatData] = []
        for i in 0..<1{
            let newChat:ChatData = ChatData.init()
            newChat.m_eChatDataType = ChatData.ChatDataType.ChatData_Message
            newChat.id = i
            newChat.m_bIsMine = false
            
            newChat.m_sMessage = "mensaje test"
            
            
            section6.append(newChat)
        }
        
        var section7:[ChatData] = []
        for i in 0..<1{
            let newChat:ChatData = ChatData.init()
            newChat.m_eChatDataType = ChatData.ChatDataType.ChatData_Image
            newChat.id = i
            newChat.m_bIsMine = false
            newChat.imgName = "company"
            newChat.m_sMessage = ""
            
            
            section7.append(newChat)
        }
        var section8:[ChatData] = []
        for i in 0..<1{
            let newChat:ChatData = ChatData.init()
            newChat.m_eChatDataType = ChatData.ChatDataType.ChatData_Image
            newChat.id = i
            newChat.m_bIsMine = true
            newChat.imgName = "company"
            newChat.m_sMessage = ""
            
            
            section8.append(newChat)
        }
        m_aMessage.append(section5)
        m_aMessage.append(section6)
        m_aMessage.append(section7)
        m_aMessage.append(section8)
        tablavista.reloadData()
        
    }
    
    @IBAction func img_button(_ sender: Any) {
        let alertController = UIAlertController(title: title, message: "Donde quieres ir?", preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "Galeria", style: .default, handler: handlerOptionGal)
        alertController.addAction(defaultAction)
        let secondAction = UIAlertAction(title: "Camera", style: .default, handler: handlerOptionCam)
        alertController.addAction(secondAction)
        present(alertController, animated: true, completion: nil)
        
    }
    
    func handlerOptionGal(alert:UIAlertAction!){
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(imagePicker, animated: true, completion: nil)
    }
    func handlerOptionCam(alert:UIAlertAction!){
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .camera
        imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(imagePicker, animated: true, completion: nil)
    }
    //MARK: - Delegates
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        var section9:[ChatData] = []
        for i in 0..<1{
            let newChat:ChatData = ChatData.init()
            newChat.m_eChatDataType = ChatData.ChatDataType.ChatData_Image
            newChat.id = i
            newChat.m_bIsMine = true
            newChat.m_Image = image
            newChat.m_sMessage = ""
            
            section9.append(newChat)
        }

        m_aMessage.append(section9)
        dismiss(animated:true, completion: nil)
        tablavista.reloadData()
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return m_aMessage.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return m_aMessage[section].count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let chat = m_aMessage[indexPath.section][indexPath.row]
        
        
        if chat.m_eChatDataType == ChatData.ChatDataType.ChatData_Message
        {
            if(chat.m_bIsMine){
                let cell = tableView.dequeueReusableCell(withIdentifier: "Chat_MyMessage_Cell", for: indexPath)
                let myCell = cell as! Chat_MyMessage_Cell
                myCell.id = chat.id
                myCell.contentLabel.text = chat.m_sMessage
                myCell.vc = self
            
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Chat_OtherMessage_Cell", for:indexPath)
                let myCell = cell as! Chat_OtherMessage_Cell
                
                myCell.contentLabel.text = chat.m_sMessage
                
                return cell
            }
        }else if chat.m_eChatDataType == ChatData.ChatDataType.ChatData_Image
        {
            if(chat.m_bIsMine){
                if(chat.imgName != ""){
                    let cell = tableView.dequeueReusableCell(withIdentifier: "Chat_MyImage_Cell", for:indexPath)
                    let myCell = cell as! Chat_MyImage_Cell
                    myCell.myImg.image = UIImage(named:chat.imgName)
                    return cell
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "Chat_MyImage_Cell", for:indexPath)
                    let myCell = cell as! Chat_MyImage_Cell
                    myCell.myImg.image = chat.m_Image
                    return cell
                }
                
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Chat_OtherImage_Cell", for:indexPath)
                let myCell = cell as! Chat_OtherImage_Cell
                myCell.otherImg.image = UIImage(named:chat.imgName)
                return cell
            }
            
        }
        else{
            chat.m_eChatDataType = ChatData.ChatDataType.ChatData_None
            let cell = tableView.dequeueReusableCell(withIdentifier: "Chat_MyMessage_Cell", for: indexPath)
            let myCell = cell as! Chat_MyMessage_Cell
            myCell.id = chat.id
            myCell.contentLabel.text = ""
            myCell.vc = self
            
            return cell
            
        }
        
    }
    
    
    
}

