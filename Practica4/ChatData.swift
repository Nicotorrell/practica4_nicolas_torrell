

import UIKit
import Foundation



class ChatData {
    
    enum ChatDataType{
        case ChatData_Message
        case ChatData_Image
        case ChatData_None
    }

    var m_bIsMine:Bool;
    var m_eChatDataType:ChatDataType;
    var m_sMessage:String;
    var m_Image:UIImage?;
    var imgName:String;
    var id:Int;
    
    init(){
        id = 0
        m_eChatDataType = ChatDataType.ChatData_Message
        m_sMessage = ""
        m_bIsMine = true
        imgName = ""
        m_Image = nil
        
    }
}
